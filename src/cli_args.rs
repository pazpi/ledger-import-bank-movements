use clap::{crate_authors, crate_description, crate_name, crate_version, App, Arg, ArgMatches};

pub fn get_arg_matches() -> ArgMatches {
    let matches = App::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!())
        .about(crate_description!())
        .arg(
            Arg::new("INPUT")
                .about("Sets the input file to use")
                .required(true)
                .index(1),
        )
        .arg(
            Arg::new("config")
                .short('c')
                .long("config")
                .value_name("FILE")
                .about("Sets a custom config file")
                .takes_value(true),
        )
        // .arg(
        //     Arg::new("v")
        //         .short('v')
        //         .multiple(true)
        //         .takes_value(true)
        //         .about("Sets the level of verbosity"),
        // )
        .arg(
            Arg::new("bank")
                .short('b')
                .long("bank")
                .required(false)
                .value_name("bank")
                .about("Name from settings.toml")
                .takes_value(true),
        )
        .arg(
            Arg::new("payees-file")
                .short('p')
                .long("payees")
                .required(false)
                .value_name("payees-file")
                .about("Ledger file with payee definitions")
                .takes_value(true),
        )
        .arg(
            Arg::new("ledger-main-file")
                .short('l')
                .long("ledger-main-file")
                .required(false)
                .value_name("ledger-file")
                .about("Ledger main file")
                .takes_value(true),
        )
        .arg(
            Arg::new("save-ledger-file")
                .short('s')
                .long("save")
                .required(false)
                .value_name("save-ledger-file")
                .about("Save ledger output to a file")
                .takes_value(true),
        )
        .arg(
            Arg::new("quiet")
                .short('q')
                .long("quiet")
                .required(false)
                .value_name("quiet")
                .about("Do not print to stdout")
                .takes_value(false),
        )
        .get_matches();

    return matches;
}

// pub struct CliArgs {
//     pub input_file: &Path,
//     pub config: Option<String>,
//     pub bank: Option<String>,
//     pub payees: Option<String>,
//     pub ledger_file: Option<String>,
//     pub safe_file: Option<String>,
// }

// impl CliArgs {
//     fn get_input(value: &Option<str>) -> &Path {
//         match value {
//             Some(name) => Path::new(name),
//             None => panic!("Nessun file di input fornito"),
//         }
//     }
//
//     fn get(value: &Option<str>) -> Option<String>
//
//     pub fn compile() -> CliArgs {
//         let matches = get_arg_matches();
//         CliArgs { config: None }
//     }
// }
