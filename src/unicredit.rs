use crate::ledger::{BankTrait, LedgerCSV, LedgerCSVRecord};
use crate::settings::BankConfig;
use csv::Trim;
use serde::{Deserialize, Serialize};
use std::path::Path;

#[derive(Serialize, Deserialize, Debug)]
pub struct UnicreditRecord {
    #[serde(rename = "Data Registrazione")]
    pub registration_date: String,
    #[serde(rename = "Data valuta")]
    pub actual_date: String,
    #[serde(rename = "Descrizione")]
    pub description: String,
    #[serde(rename = "Importo (EUR)")]
    pub value: String,
}

impl UnicreditRecord {
    fn create_ledger_record(&self, currency: &String) -> LedgerCSVRecord {
        LedgerCSVRecord {
            registration_date: self.registration_date.clone(),
            actual_date: self.actual_date.clone(),
            description: self.description.clone(),
            value: format!("{} {}", currency, self.value.clone()),
        }
    }
}

impl BankTrait for UnicreditRecord {
    fn read_source_file(path: &Path, config: &BankConfig) -> LedgerCSV {
        match path.extension().and_then(|s| s.to_str()) {
            Some("csv") => (),
            _ => {
                eprintln!("Expecting a CSV file");
                return LedgerCSV::new();
            }
        };

        let delimiter: u8 = match config.csv_separator {
            Some(delimiter) => delimiter as u8,
            None => b';',
        };

        let default_currency = String::from("€");
        let currency = config.currency.as_ref().unwrap_or(&default_currency);

        let mut csv_reader = csv::ReaderBuilder::new()
            .delimiter(delimiter)
            .trim(Trim::All)
            .from_path(path)
            .expect("File non trovato");

        let header = match csv_reader.headers() {
            Ok(record) => UnicreditRecord {
                registration_date: String::from(record.get(0).unwrap_or("NA")),
                actual_date: "".to_string(),
                description: "".to_string(),
                value: "".to_string(),
            }
            .create_ledger_record(&currency),
            Err(err) => panic!("Errore lettura header Unicredit.\n{}", err),
        };

        let mut ledger_records: Vec<LedgerCSVRecord> = Vec::new();

        for record in csv_reader.deserialize() {
            let record: UnicreditRecord = record.unwrap();
            ledger_records.push(record.create_ledger_record(&currency));
        }

        return LedgerCSV {
            header,
            records: ledger_records,
        };
    }
}
