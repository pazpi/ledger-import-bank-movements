mod cli_args;
mod ledger;
mod poste_italiane;
mod settings;
mod unicredit;

use crate::ledger::BankTrait;
use crate::settings::BankConfig;
use std::fs::OpenOptions;
use std::io;
use std::io::Write;
use std::path::Path;
use std::process::Command;

// fn _proof_of_concept() {
//     let path = Path::new("/home/pazpi/Downloads/Elenco_Movimenti.csv");
//     let mut csv_reader = csv::ReaderBuilder::new()
//         .delimiter(b';')
//         .from_path(path)
//         .expect("File non trovato");
//
//     let mut wtr = csv::Writer::from_path(Path::new("/home/pazpi/Downloads/pippo.csv"))
//         .expect("Impossibile creare file writer");
//
//     wtr.write_record(&["posted", "date", "desc", "amount"])
//         .expect("Impossibile scrivere header su file");
//
//     for record in csv_reader.deserialize() {
//         let record: unicredit::UnicreditRecord = record.unwrap();
//
//         wtr.write_record(&[
//             record.actual_date,
//             record.registration_date,
//             record.description.trim().to_string(),
//             format!("€ {}", record.value),
//         ])
//         .expect("Impossibile serializzare file");
//     }
// }

fn write_ledger_output(file_path: Option<&str>, text: Vec<u8>) {
    let path = match file_path {
        Some(path) => Path::new(path),
        None => return (),
    };

    match OpenOptions::new().create(true).append(true).open(path) {
        Ok(mut file) => {
            if let Err(e) = file.write_all(&text) {
                eprintln!(
                    "Error while writing to the output file \"{}\"\n\nErr: {}",
                    file_path.unwrap(),
                    e
                )
            }
        }
        Err(e) => {
            eprintln!("Error while opening output file.\n\nErr: {}", e);
            return ();
        }
    };
}

fn main() {
    // Compilo gli argomenti CLI
    let arg_matches = cli_args::get_arg_matches();

    let input_file = match arg_matches.value_of("INPUT") {
        Some(name) => Path::new(name),
        None => {
            eprintln!("Nessun file di input fornito");
            std::process::exit(1)
        }
    };

    let config_path = match arg_matches.value_of("config") {
        Some(path) => path,
        None => "settings.toml".clone(),
    };

    let settings = settings::Settings::read_config(config_path);

    let bank_name = match arg_matches.value_of("bank") {
        Some(name) => name,
        None => {
            eprintln!("Nessuna banca indicata");
            std::process::exit(2);
        }
    };

    // Cerco nel file di configurazione la banca
    let available_banks: Vec<BankConfig> = settings
        .bank
        .into_iter()
        .filter(|x| x.name == String::from(bank_name))
        .collect();

    // Controllo quante configurazioni ho trovato. Devono essere strettamente 1
    let bank_config: &BankConfig = match available_banks.len() {
        0 => {
            eprintln!("Banca non trovata nel file di configurazione");
            std::process::exit(3)
        }
        1 => available_banks.get(0).unwrap(),
        2 | _ => {
            eprintln!("Troppe configurazioni con questo nome");
            std::process::exit(4)
        }
    };

    eprintln!("Parsing {} file", bank_name);

    let ledger_movements = match bank_config.name.as_str() {
        "unicredit" => unicredit::UnicreditRecord::read_source_file(input_file, bank_config),
        "poste" => poste_italiane::PosteItalianeRecord::read_source_file(input_file, bank_config),
        _ => panic!("Banca ancora da implementare"),
    };

    // Scrivo il file
    let parse_csv_name = format!(".{}_parsed.csv", bank_name);
    let parse_csv_name_path = Path::new(parse_csv_name.as_str());
    ledger_movements.write_csv(parse_csv_name_path);

    let mut args_vec: Vec<&str> = vec![
        "convert",
        parse_csv_name.as_str(),
        "--rich-data",
        // "--auto-match",
    ];

    // Gerarchia:
    //  - [arg] l -> file ledger, parametro cli
    //  - [setting] ledger_file
    //  - [arg] p -> file ledger payees, parametro cli
    //  - [setting] payees_file
    let ledger_file = match arg_matches.value_of("ledger-main-file") {
        Some(file) => file.clone(),
        None => match settings.ledger.ledger_file {
            Some(ref file) => file.as_str().clone(),
            None => match arg_matches.value_of("payees-file") {
                Some(file) => file.clone(),
                None => match settings.ledger.payees_file {
                    Some(ref file) => file.as_str().clone(),
                    None => "",
                },
            },
        },
    };

    dbg!(ledger_file);

    match ledger_file.is_empty() {
        true => (),
        false => {
            args_vec.push("-f");
            args_vec.push(ledger_file.clone())
        }
    }

    match bank_config.account_name {
        Some(ref account) => {
            args_vec.push("--account");
            args_vec.push(account.as_str());
        }
        None => (),
    }

    match bank_config.date_format {
        Some(ref date_fmt) => {
            args_vec.push("--input-date-format");
            args_vec.push(date_fmt.as_str());
        }
        None => (),
    }

    let ledger_command = Command::new("ledger")
        .args(&args_vec)
        .output()
        .unwrap_or_else(|err| {
            eprintln!("Comando ledger fallito!\n\nErr: {}", err);
            std::process::exit(5);
        });

    // If ledger command fails, print the output to stderr
    if ledger_command.status.code().unwrap_or(0) > 0 {
        io::stderr().write_all(&ledger_command.stderr).unwrap();
    }

    // If not quiet print the ledger parsed file to stdout
    if !arg_matches.is_present("quiet") {
        io::stdout().write_all(&ledger_command.stdout).unwrap();
    }

    // if option -s is set, append content to that file
    write_ledger_output(
        arg_matches.value_of("save-ledger-file"),
        ledger_command.stdout,
    );
}
