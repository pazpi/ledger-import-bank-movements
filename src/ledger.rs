use crate::settings::BankConfig;
use std::path::Path;

// Valori validi per ledger
pub struct LedgerCSVRecord {
    pub registration_date: String,
    pub actual_date: String,
    pub description: String,
    pub value: String,
}

pub struct LedgerCSV {
    pub header: LedgerCSVRecord,
    pub records: Vec<LedgerCSVRecord>,
}

impl LedgerCSVRecord {
    pub fn new() -> LedgerCSVRecord {
        LedgerCSVRecord {
            registration_date: "".to_string(),
            actual_date: "".to_string(),
            description: "".to_string(),
            value: "".to_string(),
        }
    }
}

impl LedgerCSV {
    pub fn new() -> LedgerCSV {
        LedgerCSV {
            header: LedgerCSVRecord::new(),
            records: vec![],
        }
    }

    pub fn write_csv(&self, output_name: &Path) {
        let mut wtr = csv::Writer::from_path(output_name).expect("Impossibile creare file writer");

        wtr.write_record(&["posted", "date", "desc", "amount"])
            .expect("Impossibile scrivere header su file");

        for record in self.records.iter() {
            wtr.write_record(&[
                &record.actual_date,
                &record.registration_date,
                &record.description.trim().to_string(),
                &record.value,
            ])
            .expect("Impossibile serializzare file");
        }
    }
}

pub trait BankTrait {
    fn read_source_file(path: &Path, config: &BankConfig) -> LedgerCSV;
}
