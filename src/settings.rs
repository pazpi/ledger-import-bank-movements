use serde::Deserialize;
use std::fs;

#[derive(Deserialize, Debug)]
pub struct Settings {
    pub ledger: LedgerConfig,
    pub bank: Vec<BankConfig>,
}

#[derive(Deserialize, Debug)]
pub struct LedgerConfig {
    pub payees_file: Option<String>,
    pub ledger_file: Option<String>,
}

#[derive(Deserialize, Debug)]
pub struct BankConfig {
    pub convert_header: Option<Vec<String>>,
    pub csv_separator: Option<char>,
    pub currency: Option<String>,
    pub ignored_header_line: Option<usize>,
    pub name: String,
    pub excel_worksheet: Option<String>,
    pub date_format: Option<String>,
    pub account_name: Option<String>,
}

impl Settings {
    pub fn read_config(path_str: &str) -> Settings {
        // Leggo il file di configurazione
        let settings_file_contents = fs::read_to_string(path_str).unwrap_or_else(|err| {
            eprintln!(
                "Percorso File configurazione \"{}\" invalido.\n\nErrore: {}",
                path_str, err
            );
            std::process::exit(3)
        });

        // Parsing del file di configurazione
        let settings: Settings =
            toml::from_str(settings_file_contents.as_str()).unwrap_or_else(|err| {
                eprintln!("File configurazione non valido.\n\n{}", err);
                std::process::exit(4)
            });

        return settings;
    }
}
