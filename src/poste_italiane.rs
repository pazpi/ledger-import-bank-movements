use crate::ledger::{BankTrait, LedgerCSV, LedgerCSVRecord};
use crate::settings::BankConfig;
use calamine::{open_workbook_auto, RangeDeserializerBuilder, Reader};
use serde::{Deserialize, Serialize};
use std::path::{Path, PathBuf};

#[derive(Serialize, Deserialize, Debug)]
pub struct PosteItalianeRecord {
    #[serde(rename = "Data Contabile")]
    pub registration_date: String,
    #[serde(rename = "Data Valuta")]
    pub actual_date: String,
    #[serde(rename = "Addebiti (euro)")]
    pub addebito: String,
    #[serde(rename = "Accrediti (euro)")]
    pub accredito: String,
    #[serde(rename = "Descrizione operazioni")]
    pub description: String,
}

impl PosteItalianeRecord {
    fn create_ledger_record(&self, currency: &String) -> LedgerCSVRecord {
        let value = match &self.addebito.is_empty() {
            true => format!("-{}", self.accredito),
            false => self.addebito.clone(),
        };

        LedgerCSVRecord {
            registration_date: self.registration_date.clone(),
            actual_date: self.actual_date.clone(),
            description: self.description.clone().trim().parse().unwrap(),
            value: format!("{} {}", currency, value),
        }
    }
}

impl BankTrait for PosteItalianeRecord {
    fn read_source_file(path: &Path, config: &BankConfig) -> LedgerCSV {
        let sce: PathBuf = PathBuf::from(path);
        match sce.extension().and_then(|s| s.to_str()) {
            Some("xlsx") | Some("xlsm") | Some("xlsb") | Some("xls") => (),
            _ => {
                eprintln!("Expecting an EXCEL file");
                return LedgerCSV::new();
            }
        };

        let mut xl = open_workbook_auto(&sce).unwrap();
        let defeault_worksheet = xl
            .sheet_names()
            .get(0)
            .expect("Nessun worksheet presente nel file")
            .clone();

        let worksheet = match &config.excel_worksheet {
            Some(worksheet) => worksheet,
            None => &defeault_worksheet,
        };

        let skipping_rows = config.ignored_header_line.unwrap_or(0);

        let mut range = xl.worksheet_range(worksheet.as_str()).unwrap().unwrap();
        let size = range.get_size();

        range = range.range((skipping_rows as u32, 0), (size.0 as u32, size.1 as u32));

        let iter_result = RangeDeserializerBuilder::new()
            .has_headers(true)
            .from_range::<_, PosteItalianeRecord>(&range)
            .expect("Errore deserializzando Poste Italiane records");

        let default_currency = String::from("€");
        let currency = config.currency.as_ref().unwrap_or(&default_currency);
        let mut ledger_records: Vec<LedgerCSVRecord> = Vec::new();

        for (index, row) in iter_result.enumerate() {
            match row {
                Ok(row) => {
                    ledger_records.push(row.create_ledger_record(&currency));
                }
                Err(row) => eprintln!("{}: {:?}", index, row),
            }
        }

        return LedgerCSV {
            header: LedgerCSVRecord::new(),
            records: ledger_records,
        };
    }
}
