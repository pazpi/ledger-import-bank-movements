# Leder import transactions

This utility has been created to automatically convert the transcations history into a
valid CSV format that [ledger](https://ledger-cli.org) can import.

``` shell
./parse_bank_movements <INPUT_FILE> <OUTPUT_FILE> -b <bank_name>
ledger convert <OUTPUT_FILE>
```

## Config file

For now the application will search for a file named `settings.toml`.
This file containes the settings that will be used.

Each bank is a table entry in a TOML file. These are the available settings

* `name`: bank name. This is the name used with the `-b` parameter

* `csv_separator`

* `ignored_header_line`
  
* `currency`

* `excel_worksheet`

## Next features

This first release is functional but there are still huge chunk missing, for istance
the name of the output file (for now you will see a _pippo.csv_ file created).
Since this is a program shaped around my needs, of course you will find some constraints.
But do not hesitate. Leave me a note!

Anyway, this is the future roadmap of my next feature:

* set output file
* run ledger commands directly
* handle ledger payee (usefull to only check _Unknown_ transaction)
* docs
* integration/unit tests

## Note

This is my first attempt at writing something in [Rust](https://rust-lang.org).
It is not perfect and for sure there are huge chunks of "bad code" or "messy" parts.
My goal is to develop features that I consider usefull for my accounting workflow.
If you would like has something implemented do not hesitate to open an issue.